@extends('layouts.frontend.app',[
    'title' => 'Topik Makalah',
])
@section('content')
<div class="regular-page-area section-padding-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-content">
                    {{-- <h4>Topik Makalah</h4> --}}
                    <table class="table table-hover table-bordered" style="font-size: 15pt">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Topik Makalah</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Teknologi dan Sistem Informasi</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Telekomunikasi dan Telematika</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Sistem Kendali dan Elektronika</td>
                          </tr>
                          <tr>
                            <th scope="row">4</th>
                            <td>Teknik Tenaga Listrik</td>
                          </tr>
                          <tr>
                            <th scope="row">5</th>
                            <td>Konversi Energi</td>
                          </tr>
                          <tr>
                            <th scope="row">6</th>
                            <td>Teknologi Bahan dan Material Maju</td>
                          </tr>
                          <tr>
                            <th scope="row">7</th>
                            <td>Refrigerasi dan Tata Udara</td>
                          </tr>
                          <tr>
                            <th scope="row">8</th>
                            <td>Produksi dan Perancangan</td>
                          </tr>
                          <tr>
                            <th scope="row">9</th>
                            <td>Teknik dan Manajemen Industri</td>
                          </tr>
                          <tr>
                            <th scope="row">10</th>
                            <td>Rekayasa Struktur Bangunan</td>
                          </tr>
                          <tr>
                            <th scope="row">11</th>
                            <td>Jalan dan Jembatan</td>
                          </tr>
                          <tr>
                            <th scope="row">12</th>
                            <td>Hidrologi dan Sumber Daya Air</td>
                          </tr>
                          <tr>
                            <th scope="row">13</th>
                            <td>Teknik dan Manajemen Lingkungan</td>
                          </tr>
                          <tr>
                            <th scope="row">14</th>
                            <td>Pengendalian Pencemaran</td>
                          </tr>
                          <tr>
                            <th scope="row">15</th>
                            <td>E-Commerce</td>
                          </tr>
                          <tr>
                            <th scope="row">16</th>
                            <td>Pemberdayaan UMKM</td>
                          </tr>
                          <tr>
                            <th scope="row">17</th>
                            <td>Pengembangan Produk Unggulan</td>
                          </tr>
                          <tr>
                            <th scope="row">18</th>
                            <td>Strategi Pemasaran</td>
                          </tr>
                          <tr>
                            <th scope="row">19</th>
                            <td>Bisnis dan Enterpreneurship</td>
                          </tr>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop