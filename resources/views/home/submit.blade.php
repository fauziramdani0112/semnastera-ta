@extends('layouts.frontend.app',[
    'title' => 'Submit',
])
@section('content')
<!-- ##### Blog Area Start ##### -->
<section class="blog-area section-padding-100-0 mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading">
                    <h3>Submit</h3>
                </div>
            </div>
        </div>

        <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                            PANDUAN SUBMIT
                        </div>
                        <div class="card-body">
                            Silakan download panduan lewat tautan di bawah ini
                            <br><br>
                            <a href="https://semnastera.polteksmi.ac.id/2022/images/Panduan_Pemakalah_dan_Non_Pemakalah.pdf" target="_blank" class="btn btn-primary btn-sm">Download Panduan</a>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                            SUBMIT MAKALAH
                        </div>
                        <div class="card-body">
                            Silakan submit makalah anda lewat tautan di bawah ini
                            <br><br>
                            <a href="https://semnastera.polteksmi.ac.id/index.php/semnastera/user/register" target="_blank" class="btn btn-primary btn-sm">Submit Makalah</a>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                            TEMPLATE MAKALAH
                        </div>
                        <div class="card-body">
                            Silakan download template makalah lewat tautan di bawah ini
                            <br><br>
                            <a href="https://semnastera.polteksmi.ac.id/2022/images/Template_Semnastera.docx" class="btn btn-primary btn-sm">Download Template</a>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                            PANDUAN TEKNIS PRESENTASI
                        </div>
                        <div class="card-body">
                            Silakan download panduan teknis lewat tautan di bawah ini
                            <br><br>
                            <a href="https://semnastera.polteksmi.ac.id/2022/images/PANDUAN%20TEKNIS%20PRESENTASI%20SEMNASTERA.pdf" target="_blank" class="btn btn-primary btn-sm">Download Panduan</a>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
@stop